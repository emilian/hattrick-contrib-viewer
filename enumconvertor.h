/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef PARSER_H
#define PARSER_H

#include <QString>

#include "enums.h"
#include "enumconvertorinnards.h"

/* Provides enum <-> QString conversion utilities */
class EnumConvertor
{
public:
    EnumConvertor();

    static QString toString(const PlayerIndividualOrder order);
    static QString toString(const PlayerPosition position);
    static QString toString(const PlayerSide side);
    static QString toString(const PlayerSkill skill);

    static PlayerIndividualOrder ordFromString(const QString& string);
    static PlayerPosition posFromString(const QString& string);
    static PlayerSide sidFromString(const QString& string);
    static PlayerSkill sklFromString(const QString& string);

private:
    /* We're using a static instance here because it's the only
     * remotely sane way I found how to make sure the "enum <-> string"
     * hash maps get properly initialized (since they're static).
     *
     * This is *not* meant to be a regular C++ pimpl. */
    static EnumConvertorInnards innards;
};

#endif // PARSER_H
