/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "pitchareahighlight.h"

#include <QtDebug>

#include <QPainter>
#include <QPen>

#include <algorithm>    // qSort() is deprecated, recommendation is to use std::sort instead.

#include "enumconvertor.h"

PitchAreaHighlight::PitchAreaHighlight(const PitchArea pitchArea,
                                       const QRectF &pitchBoundingRect) :
    pitchArea(pitchArea),
    contributionDetailPen(new QPen(QColor(Qt::white)))
{
    qreal areaTopLeftXCoord = 0.0, areaTopLeftYCoord = 0.0;
    qreal areaWidth = 0.0, areaHeight = 0.0;

    qreal oneThirdPitchHeight = pitchBoundingRect.height() / 3.0;
    qreal oneThirdPitchWidth = pitchBoundingRect.width() / 3.0;

    switch (pitchArea) {
    case PA_CENTRAL_DEFENSE:
    case PA_CENTRAL_ATTACK:
        areaTopLeftYCoord = oneThirdPitchHeight;
        break;
    case PA_RIGHT_DEFENSE:
    case PA_RIGHT_ATTACK:
        areaTopLeftYCoord = 2.0 * oneThirdPitchHeight;
        break;
    default:
        break;  // avoid 'enumeration value not handled' compiler warnings
    }

    switch(pitchArea) {
    case PA_MIDFIELD:
        areaTopLeftXCoord = oneThirdPitchWidth;
        break;
    case PA_LEFT_ATTACK:
    case PA_CENTRAL_ATTACK:
    case PA_RIGHT_ATTACK:
        areaTopLeftXCoord = 2.0 * oneThirdPitchWidth;
        break;
    default:
        break;  // avoid 'enumeration value not handled' compiler warnings
    }

    // this is the same for all pitch areas
    areaWidth = oneThirdPitchWidth;

    switch (pitchArea) {
    case PA_MIDFIELD:
        areaHeight = pitchBoundingRect.height();
        break;
    default:
        areaHeight = oneThirdPitchHeight;
    }

    boundingRectangle = QRectF(areaTopLeftXCoord, areaTopLeftYCoord,
                               areaWidth, areaHeight);
}

PitchAreaHighlight::~PitchAreaHighlight()
{
    delete contributionDetailPen;
    contributionDetailPen = 0;
}

QRectF PitchAreaHighlight::boundingRect() const
{
    return boundingRectangle;
}

// Comparator for 'Contribution' objects
bool contributionGreaterThan(const Contribution& c1, const Contribution& c2) {
    return c1.contributionValue > c2.contributionValue;
}

void PitchAreaHighlight::paint(QPainter *painter,
                               const QStyleOptionGraphicsItem * /* option */,
                               QWidget * /* widget */)
{
    int totalContribValue = 0;
    for (Contribution c: activeContributions) {
        totalContribValue += c.contributionValue;
    }

    int alphaComponent = 255;   // 100% opaque
    if (!activeContributions.isEmpty())
        alphaComponent = 196;


    painter->fillRect(boundingRect(), QBrush(QColor(totalContribValue, totalContribValue, totalContribValue, alphaComponent)));

    int vertOffset = 20;
    for (Contribution c: activeContributions) {
        painter->setPen(*contributionDetailPen);
        QPointF textTopLeft = boundingRectangle.topLeft() + QPointF(0, vertOffset);
        QString text = EnumConvertor::toString(c.contributingSkill) + " (" +
                        QString::number(c.contributionValue) + "%)";
        painter->drawText(textTopLeft, text);
        vertOffset += 20;
    }
}

void PitchAreaHighlight::setActiveContributions(const QList<Contribution> &activeContributions)
{
    this->activeContributions = activeContributions;
    std::sort(this->activeContributions.begin(),
              this->activeContributions.end(),
              contributionGreaterThan);
    update();
}
