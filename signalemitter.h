/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNALEMITTER_H
#define SIGNALEMITTER_H

#include <QObject>

enum class PlayerIndividualOrder : int;
enum class PlayerPosition : int;
enum class PlayerSide : int;

class PlayerItem;

/* Emits signals on behalf of PlayerItems.
 * Needed because QGraphicsItem does not subclass QObject.
 *
 * ref: http://www.qtcentre.org/threads/12630-QGraphicsItem-doesn-t-inherit-QObject
 */
class SignalEmitter : public QObject
{
    Q_OBJECT
public:
    explicit SignalEmitter(QObject *parent = 0);

    void emitRatingsChanged(PlayerItem* emitter);

signals:
    /* Emitted whenever the ratings displayed on the pitch need to change.
     * Can happen either because a new player has been selected, or the
     * player instructions have been changed (e.g. currently-selected player
     * was made to play towards the wing). */
    void ratingsChanged(PlayerItem* emitter);
};

#endif // SIGNALEMITTER_H
