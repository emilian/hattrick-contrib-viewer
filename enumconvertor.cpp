/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "enumconvertor.h"

EnumConvertorInnards EnumConvertor::innards;

EnumConvertor::EnumConvertor()
{

}

QString EnumConvertor::toString(const PlayerIndividualOrder order)
{
    return innards.toString(order);
}

QString EnumConvertor::toString(const PlayerPosition position)
{
    return innards.toString(position);
}

QString EnumConvertor::toString(const PlayerSide side)
{
    return innards.toString(side);
}

QString EnumConvertor::toString(const PlayerSkill skill)
{
    return innards.toString(skill);
}

PlayerIndividualOrder EnumConvertor::ordFromString(const QString &string)
{
    return innards.ordFromString(string);
}

PlayerPosition EnumConvertor::posFromString(const QString &string)
{
    return innards.posFromString(string);
}

PlayerSide EnumConvertor::sidFromString(const QString &string)
{
    return innards.sidFromString(string);
}

PlayerSkill EnumConvertor::sklFromString(const QString &string)
{
    return innards.sklFromString(string);
}


