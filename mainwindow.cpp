/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtDebug>

#include <QFileInfo>
#include <QGraphicsScene>
#include <QGraphicsSvgItem>
#include <QMap>

#include "enumconvertor.h"
#include "pitchareahighlight.h"
#include "playeritem.h"
#include "ratingmapper.h"
#include "signalemitter.h"

const QString MainWindow::EXTERNAL_DATA_FILE_NAME = "player_contributions.csv";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    scene(new QGraphicsScene(this)),
    pitch(new QGraphicsSvgItem(":gfx/pitch.svg")),
    selectedPlayer(0),
    ratingMapper(0),
    pitchAreaMap(new QMap<PitchArea, PitchAreaHighlight *>()),
    playerCoordinatesMap(new QMap<PlayerItem*, QPointF>())
{
    ui->setupUi(this);
    ui->graphicsView->setScene(scene);

    scene->addItem(pitch);
    for (int area = PitchArea::PA_RIGHT_DEFENSE; area < PitchArea::PA_END; ++area) {
        PitchAreaHighlight *pah = new PitchAreaHighlight(static_cast<PitchArea>(area),
                                                         pitch->boundingRect());
        pitchAreaMap->insert(static_cast<PitchArea>(area), pah);
        scene->addItem(pah);
    }

    QFileInfo fileInfo(EXTERNAL_DATA_FILE_NAME);
    bool externalFileLoaded = false;
    if (fileInfo.exists() && fileInfo.isFile()) {
        ratingMapper = new RatingMapper(EXTERNAL_DATA_FILE_NAME);
        if (ratingMapper->fileOpenedSuccessfully()) {
            externalFileLoaded = true;
        } else {
            delete ratingMapper;
            ratingMapper = 0;
        }
    }
    if (!externalFileLoaded) {
        ratingMapper = new RatingMapper(":data/player_contributions_season_59.csv");
    }

    QList<PlayerPosition> positions;

    // goalkeeper
    positions.append(PlayerPosition::GOALKEEPER);
    setupPlayerLine(55, 1, positions);
    positions.clear();

    // defenders
    positions.append(PlayerPosition::CENTRAL_DEFENDER);
    positions.append(PlayerPosition::WINGBACK);
    setupPlayerLine(230, 5, positions);
    positions.clear();

    // midfielders
    positions.append(PlayerPosition::INNER_MIDFIELDER);
    positions.append(PlayerPosition::WINGER);
    setupPlayerLine(525, 5, positions);
    positions.clear();

    // forwards
    positions.append(PlayerPosition::FORWARD);
    setupPlayerLine(850, 3, positions);

    connect(PlayerItem::getEmitter(), &SignalEmitter::ratingsChanged,
            this, &MainWindow::handlePlayerSelection);
}


MainWindow::~MainWindow()
{
    delete playerCoordinatesMap;
    playerCoordinatesMap = 0;
    delete pitchAreaMap;
    pitchAreaMap = 0;
    delete ratingMapper;
    ratingMapper = 0;
    delete ui;
    ui = 0;
}

/* Updates the pitch display with the respective contributions of the newly-selected player */
void MainWindow::handlePlayerSelection(PlayerItem *emitter)
{
    if (emitter == 0) {
        qDebug() << "Warning: player selection slot called in invalid scenario!";
        return;
    }

    if (selectedPlayer != emitter) {
        if (selectedPlayer != 0) {
            selectedPlayer->deselect();
        }

        selectedPlayer = emitter;
    }

    QLinkedList<Contribution> contributions =
            ratingMapper->getContributionForPlayer(emitter->getPosition(),
                                                   emitter->getSide(),
                                                   emitter->getIndividualOrder());

    for (int area = PitchArea::PA_RIGHT_DEFENSE; area < PitchArea::PA_END; ++area) {
        QList<Contribution> contributionList;
        for (Contribution c: contributions) {
            if (c.pitchArea == static_cast<PitchArea>(area))
                contributionList.append(c);
        }
        (*pitchAreaMap)[static_cast<PitchArea>(area)]->setActiveContributions(contributionList);
    }

    QPointF playerCoordinates = playerCoordinatesMap->value(selectedPlayer);
    switch (selectedPlayer->getIndividualOrder()) {
    case PlayerIndividualOrder::DEFENSIVE:
    case PlayerIndividualOrder::TECH_DEFENSIVE:
        playerCoordinates -= QPointF(INDIVIDUAL_ORDER_COORDINATE_OFFSET, 0);
        break;
    case PlayerIndividualOrder::OFFENSIVE:
        playerCoordinates += QPointF(INDIVIDUAL_ORDER_COORDINATE_OFFSET, 0);
        break;
    case PlayerIndividualOrder::TOWARDS_WING:
        switch (selectedPlayer->getSide()) {
        case PlayerSide::LEFT:
            playerCoordinates -= QPointF(0, INDIVIDUAL_ORDER_COORDINATE_OFFSET);
            break;
        case PlayerSide::RIGHT:
            playerCoordinates += QPointF(0, INDIVIDUAL_ORDER_COORDINATE_OFFSET);
            break;
        default:
            break;  // avoid 'enumeration value not handled' compiler warnings
        }
        break;
    case PlayerIndividualOrder::TO_THE_MIDDLE:
        switch (selectedPlayer->getSide()) {
        case PlayerSide::LEFT:
            playerCoordinates += QPointF(0, INDIVIDUAL_ORDER_COORDINATE_OFFSET);
            break;
        case PlayerSide::RIGHT:
            playerCoordinates -= QPointF(0, INDIVIDUAL_ORDER_COORDINATE_OFFSET);
            break;
        default:
            break;  // avoid 'enumeration value not handled' compiler warnings
        }
        break;
    default:
        break;  // avoid 'enumeration value not handled' compiler warnings
    }

    selectedPlayer->setPos(playerCoordinates);
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    fitPitchInWindow();
}

void MainWindow::showEvent(QShowEvent *)
{
    fitPitchInWindow();
}

/* Makes sure the pitch takes up the entirety of the main QGraphicsView. */
void MainWindow::fitPitchInWindow()
{
    QRectF bounds = scene->itemsBoundingRect();
    bounds.setWidth(bounds.width() * 1.1);
    bounds.setHeight(bounds.height() * 1.1);
    ui->graphicsView->fitInView(bounds, Qt::KeepAspectRatio);
}

/* Sets up all players which make up one vertical line.
 *  (e.g. all five defenders/all three strikers/the goalkeeper)
 *
 * Assumption: 'numberOfPlayers' will always be odd.
 *  (We have no line which is made up of an even number of players)
 *  (1GK - 5DEF - 5MID - 3FWD)
 *
 * Assumption: 'positions' will contain all the positions found at this level, ordered starting from the center.
 *  (e.g. for the defensive line we expect 'positions' to have 2 elements:
 *      positions[0] == PlayerPosition.CD
 *      positions[1] == PlayerPosition.WB)
 */
void MainWindow::setupPlayerLine(const int horizontalOffset,
                                 const int numberOfPlayers,
                                 const QList<PlayerPosition>& positions)
{
    if (positions.size() == 0) {
        qDebug() << "setupPlayerLine: there are no specified positions for the players!";
        return;
    }

    int pitchHeight = pitch->boundingRect().height();

    int midPlayerYCoord = pitchHeight / 2;

    // we'll always have a player positioned in the middle of the line/pitch
    PlayerPosition playerPosition = positions[0];
    PlayerSide playerSide = PlayerSide::CENTRAL;
    PlayerItem *player = new PlayerItem(playerPosition,
                                        playerSide,
                                        ratingMapper->getApplicableIndividualOrders(playerPosition, playerSide));

    int playerHeight = player->boundingRect().height();
    QPointF playerCoordinates(horizontalOffset, midPlayerYCoord - (playerHeight / 2));
    player->setPos(playerCoordinates);
    playerCoordinatesMap->insert(player, playerCoordinates);
    scene->addItem(player); // (1) after this call, 'player' will belong to 'scene' w.r.t. memory management

    // let's add the rest of the players above and below the player-in-the-middle
    for (int i=1; i <= (numberOfPlayers - 1)/2; ++i) {
        /* 1. above */
        playerPosition = positions[i-1];
        playerSide = PlayerSide::LEFT;
        // because of (1), we're free to reuse the 'player variable for a new player
        player = new PlayerItem(playerPosition,
                                playerSide,
                                ratingMapper->getApplicableIndividualOrders(playerPosition, playerSide));

        // where the player would be if players had no height
        int playerYCoord = midPlayerYCoord - i * playerHeight;

        // ...adjust for player height...
        playerYCoord -= playerHeight / 2;

        // ...and add a small offset so players are not stuck to each other.
        playerYCoord -= i * 30;

        playerCoordinates.setX(horizontalOffset);
        playerCoordinates.setY(playerYCoord);
        playerCoordinatesMap->insert(player, playerCoordinates);
        player->setPos(playerCoordinates);
        scene->addItem(player);

        /* 2. below */
        playerPosition = positions[i-1];
        playerSide = PlayerSide::RIGHT;
        // because of (1), we're free to reuse the 'player variable for a new player
        player = new PlayerItem(playerPosition,
                                playerSide,
                                ratingMapper->getApplicableIndividualOrders(playerPosition, playerSide));

        // where the first player below would be, adjusted for player height
        playerYCoord = midPlayerYCoord + playerHeight / 2;

        // ...adjust for all the players that are above the current one, but below the player-in-the-middle...
        playerYCoord += (i - 1) * playerHeight;

        // ...and add a small offset so that players are not stuck to each other.
        playerYCoord += 30 * i;

        playerCoordinates.setX(horizontalOffset);
        playerCoordinates.setY(playerYCoord);
        player->setPos(playerCoordinates);
        playerCoordinatesMap->insert(player, playerCoordinates);
        scene->addItem(player);
    }
}
