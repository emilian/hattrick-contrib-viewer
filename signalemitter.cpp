/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "signalemitter.h"

SignalEmitter::SignalEmitter(QObject *parent) : QObject(parent)
{

}

void SignalEmitter::emitRatingsChanged(PlayerItem* emitter)
{
    emit ratingsChanged(emitter);
}
