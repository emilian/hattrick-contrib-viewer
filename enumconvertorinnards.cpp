/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "enumconvertorinnards.h"
#include <QtDebug>

EnumConvertorInnards::EnumConvertorInnards()
{
    addMapping(PlayerIndividualOrder::NORMAL, "Normal");
    addMapping(PlayerIndividualOrder::DEFENSIVE, "Defensive");
    addMapping(PlayerIndividualOrder::OFFENSIVE, "Offensive");
    addMapping(PlayerIndividualOrder::TOWARDS_WING, "Towards wing");
    addMapping(PlayerIndividualOrder::TO_THE_MIDDLE, "To the middle");
    addMapping(PlayerIndividualOrder::TECH_DEFENSIVE, "Tech. Defensive");

    addMapping(PlayerPosition::GOALKEEPER, "GK");
    addMapping(PlayerPosition::CENTRAL_DEFENDER, "CD");
    addMapping(PlayerPosition::WINGBACK, "WB");
    addMapping(PlayerPosition::INNER_MIDFIELDER, "IM");
    addMapping(PlayerPosition::WINGER, "W");
    addMapping(PlayerPosition::FORWARD, "FW");

    addMapping(PlayerSide::CENTRAL, "Central");
    addMapping(PlayerSide::LEFT, "Left");
    addMapping(PlayerSide::RIGHT, "Right");

    addMapping(PlayerSkill::GOALKEEPING, "Keeper");
    addMapping(PlayerSkill::DEFENDING, "Defending");
    addMapping(PlayerSkill::PLAYMAKING, "Playmaking");
    addMapping(PlayerSkill::WINGER, "Winger");
    addMapping(PlayerSkill::PASSING, "Passing");
    addMapping(PlayerSkill::SCORING, "Scoring");

}

/* Checks made using 'contains()' are used instead of simply using operator[]
 * because operator[] silently inserts the queried value into the map if it does
 * not already contain it. */
QString EnumConvertorInnards::toString(const PlayerIndividualOrder order)
{
    if (hashOrdStr.contains(order)) {
        return hashOrdStr.value(order);
    } else {
        return "??";
    }
}

QString EnumConvertorInnards::toString(const PlayerPosition position)
{
    if (hashPosStr.contains(position)) {
        return hashPosStr.value(position);
    } else {
        return "??";
    }
}

QString EnumConvertorInnards::toString(const PlayerSide side)
{
    if (hashSidStr.contains(side)) {
        return hashSidStr[side];
    } else {
        return "??";
    }
}

QString EnumConvertorInnards::toString(const PlayerSkill skill)
{
    if (hashSklStr.contains(skill)) {
        return hashSklStr[skill];
    } else {
        return "??";
    }
}

PlayerIndividualOrder EnumConvertorInnards::ordFromString(const QString &string)
{
    if (hashStrOrd.contains(string)) {
        return hashStrOrd.value(string);
    } else {
        return PlayerIndividualOrder::NONE;
    }
}

PlayerPosition EnumConvertorInnards::posFromString(const QString &string)
{
    if (hashStrPos.contains(string)) {
        return hashStrPos.value(string);
    } else {
        return PlayerPosition::NONE;
    }
}

PlayerSide EnumConvertorInnards::sidFromString(const QString &string)
{
    if (hashStrSid.contains(string)) {
        return hashStrSid.value(string);
    } else {
        return PlayerSide::NONE;
    }
}

PlayerSkill EnumConvertorInnards::sklFromString(const QString &string)
{
    if (hashStrSkl.contains(string)) {
        return hashStrSkl.value(string);
    } else {
        return PlayerSkill::NONE;
    }
}

void EnumConvertorInnards::addMapping(const PlayerIndividualOrder order, const QString &string)
{
    hashOrdStr.insert(order, string);
    hashStrOrd.insert(string, order);
}

void EnumConvertorInnards::addMapping(const PlayerPosition position, const QString &string)
{
    hashPosStr.insert(position, string);
    hashStrPos.insert(string, position);
}

void EnumConvertorInnards::addMapping(const PlayerSide side, const QString &string)
{
    hashSidStr.insert(side, string);
    hashStrSid.insert(string, side);
}

void EnumConvertorInnards::addMapping(const PlayerSkill skill, const QString &string)
{
    hashSklStr.insert(skill, string);
    hashStrSkl.insert(string, skill);
}

/* qHash() functions needed so we can use these enums as keys in a QHash.
 *
 * Perhaps it would be better to templatize these functions.
 * C++ is fun enough without adding templates into the mix... so we won't do that yet. */
uint qHash(const PlayerIndividualOrder key, uint seed) {
    return ::qHash(static_cast<int>(key), seed);
}

uint qHash(const PlayerPosition key, uint seed) {
    return ::qHash(static_cast<int>(key), seed);
}

uint qHash(const PlayerSide key, uint seed) {
    return ::qHash(static_cast<int>(key), seed);
}

uint qHash(const PlayerSkill key, uint seed) {
    return ::qHash(static_cast<int>(key), seed);
}
