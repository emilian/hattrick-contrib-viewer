/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "ratingmapper.h"

#include <QFile>
#include <QString>

#include "enumconvertor.h"

#include <QtDebug>

RatingMapper::RatingMapper(const QString &dataFilename) :
    fileOpenedOk(false),
    lastParsedIndivOrder(PlayerIndividualOrder::NONE),
    lastParsedPosition(PlayerPosition::NONE),
    lastParsedSide(PlayerSide::CENTRAL)
{
    QFile dataFile(dataFilename);
    if (!dataFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error opening data file !" << dataFile.errorString();
        return;
    }

    dataTable.clear();
    while(!dataFile.atEnd()) {
        QByteArray line = dataFile.readLine();
        parseLine(line);
    }

    fileOpenedOk = true;
}

bool RatingMapper::fileOpenedSuccessfully() const
{
    return fileOpenedOk;
}

QLinkedList<Contribution>
RatingMapper::getContributionForPlayer(const PlayerPosition position,
                                       const PlayerSide side,
                                       const PlayerIndividualOrder order) const
{
    return dataTable.value(getDataTableKey(position, side, order));
}

QLinkedList<PlayerIndividualOrder>
RatingMapper::getApplicableIndividualOrders(const PlayerPosition position,
                                            const PlayerSide side) const
{
    QString key = getIndividualOrderTableKey(position, side);
    return applicableIndividualOrdersTable.value(key);
}

QString RatingMapper::getDataTableKey(const PlayerPosition pos,
                                      const PlayerSide side,
                                      const PlayerIndividualOrder order) const
{
    return EnumConvertor::toString(pos) + EnumConvertor::toString(side) + EnumConvertor::toString(order);
}

QString RatingMapper::getIndividualOrderTableKey(const PlayerPosition pos,
                                                 const PlayerSide side) const
{
    return EnumConvertor::toString(pos) + EnumConvertor::toString(side);
}

void RatingMapper::parseLine(const QByteArray &line)
{
    QList<QByteArray> lineContents = line.split(',');

    if (lineContents.size() != EXPECTED_LINE_ELEMENT_COUNT) {
        qDebug() << "Warning: line " << lineContents << ": expected " << EXPECTED_LINE_ELEMENT_COUNT
                 << " elements but found " << lineContents.size() << "! Skipping!";
        return;
    }

    QString playerPosition = lineContents.first();
    if (!playerPosition.isEmpty()) {
        PlayerPosition previousPosition = lastParsedPosition;
        PlayerSide previousSide = lastParsedSide;

        // determine player's side
        QString strLeft = EnumConvertor::toString(PlayerSide::LEFT);
        QString strRight = EnumConvertor::toString(PlayerSide::RIGHT);
        if (playerPosition.startsWith(strLeft)) {
            lastParsedSide = PlayerSide::LEFT;
            playerPosition.replace(0, strLeft.length(), "");
        } else if (playerPosition.startsWith(strRight)) {
            lastParsedSide = PlayerSide::RIGHT;
            playerPosition.replace(0, strRight.length(), "");
        } else {
            lastParsedSide = PlayerSide::CENTRAL;
        }

        // determine player's position
        lastParsedPosition = EnumConvertor::posFromString(playerPosition.trimmed());
        if (lastParsedPosition == PlayerPosition::NONE) {
            qDebug() << "Warning! " << playerPosition.trimmed() << " is not a valid player position!";

            // rollback
            lastParsedSide = previousSide;
            lastParsedPosition = previousPosition;
        }

        // determine player's individual order
        QString playerIndivOrder = lineContents.at(1);
        PlayerIndividualOrder lineOrder = EnumConvertor::ordFromString(playerIndivOrder);
        if (lineOrder != PlayerIndividualOrder::NONE) {
            lastParsedIndivOrder = lineOrder;
        } else {
            if (lastParsedPosition == PlayerPosition::GOALKEEPER) {
                /* Goalkeepers can't have individual orders.
                 * We'll consider them as playing normally by default. */
                lastParsedIndivOrder = PlayerIndividualOrder::NORMAL;
            } else {
                qDebug() << "Warning: " << playerIndivOrder << " could not be parsed as an individual order!";
            }
        }

    } else {
        /* We are parsing a continuation line, we should add the contributions
         * from this line directly to the player playing in the position+side we
         * parsed immediately before. */
    }

    PlayerSkill skill = EnumConvertor::sklFromString(lineContents.at(2));
    if (skill == PlayerSkill::NONE) {
        if (!lineContents.at(2).isEmpty()) {    // prevent logspam
            qDebug() << "Warning: " << lineContents.at(2) << " could not be parsed as a player skill!";
        }
        return;
    }

    QString individualOrderTableKey = getIndividualOrderTableKey(lastParsedPosition, lastParsedSide);
    if (!applicableIndividualOrdersTable[individualOrderTableKey].contains(lastParsedIndivOrder)) {
        applicableIndividualOrdersTable[individualOrderTableKey].push_back(lastParsedIndivOrder);
    }

    Contribution c;
    c.contributingSkill = skill;

    QString dataTableKey = getDataTableKey(lastParsedPosition, lastParsedSide, lastParsedIndivOrder);
    bool contribConversionOk = false;

    for (int currentArea = PA_RIGHT_DEFENSE; currentArea < PA_END; ++currentArea) {
        c.pitchArea = static_cast<PitchArea>(currentArea);
        int contribution = lineContents[3 + currentArea].replace("%", "").toInt(&contribConversionOk);
        if (contribConversionOk) {
            if (contribution != 0) {
                c.contributionValue = contribution;
                dataTable[dataTableKey].push_front(c);
            } else {
                // no sense in filling up the table with areas where the player does not contribute at all...
            }
        } else {
            qDebug() << "Warning: " << lineContents.at(3 + currentArea) << " could not be converted to a contribution value!";
        }
    }
}
