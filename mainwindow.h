/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGlobal>     // needed for QT_BEGIN/END_NAMESPACE macros

#include "enums.h"

class PitchAreaHighlight;
class PlayerItem;
class RatingMapper;

/* QT_BEGIN/END_NAMESPACE help in the case that the Qt library
 *  used to compile the project has itself been compiled with the
 *  '--qtnamespace' option (which allows Qt to be built inside a
 *  user-defined namespace. */
QT_BEGIN_NAMESPACE
class QGraphicsScene;
class QGraphicsSvgItem;

template<class Key, class Value>
class QMap;
QT_END_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void handlePlayerSelection(PlayerItem *emitter);

protected:
    void resizeEvent(QResizeEvent *) override;
    void showEvent(QShowEvent *) override;

private:
    Ui::MainWindow *ui;

    QGraphicsScene *scene;
    QGraphicsSvgItem *pitch;

    PlayerItem  *selectedPlayer;
    RatingMapper *ratingMapper;

    QMap<PitchArea, PitchAreaHighlight *> *pitchAreaMap;

    /* Stores the (x, y) coordinates at which each player is positioned
     * on the pitch when not given any individual order. */
    QMap<PlayerItem *, QPointF> *playerCoordinatesMap;

    /* The amount by which a player given a non-NORMAL individual order
     * gets moved in the order-specific direction.
     *
     * e.g. a Central Defender told to play Offensively will cause the
     * player's representation on the pitch to move towards the middle of
     * the pitch by this amount */
    static constexpr double INDIVIDUAL_ORDER_COORDINATE_OFFSET = 20.0;

    /* First location where the contribution data is searched for. Refers to a file
     * that is supposed to exist at the same directory level as the main executable.
     *
     * If the file does not exist, the application uses its built-in file instead. */
    static const QString EXTERNAL_DATA_FILE_NAME;

    void fitPitchInWindow();
    void setupPlayerLine(const int horizontalOffset,
                         const int numberOfPlayers,
                         const QList<PlayerPosition> &positions);
};

#endif // MAINWINDOW_H
