/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef ENUMS_H
#define ENUMS_H

enum PitchArea {
    /* The order of the values in this enum should correspond with
     * the order in which contributions are given in the data table.*/
    PA_RIGHT_DEFENSE, PA_CENTRAL_DEFENSE, PA_LEFT_DEFENSE,
    PA_MIDFIELD,
    PA_RIGHT_ATTACK, PA_CENTRAL_ATTACK, PA_LEFT_ATTACK,
    PA_END  // should always be the last value in the enum!
};

enum class PlayerPosition {
    NONE,   // used for 'uninitialized' values and to denote parse errors
    GOALKEEPER,
    WINGBACK, CENTRAL_DEFENDER,
    INNER_MIDFIELDER, WINGER,
    FORWARD
};

enum class PlayerSide {
    NONE,   // used for 'uninitialized' values and to denote parse errors
    LEFT, CENTRAL, RIGHT
};

enum class PlayerIndividualOrder {
    NONE,   // used for 'uninitialized' values and to denote parse errors

    NORMAL, DEFENSIVE, TOWARDS_WING, TO_THE_MIDDLE, OFFENSIVE,

    /* Technical forwards gain a small boost to a few contribution scores
     * when played defensively. The data sheet reflects this by considering
     * "Tech. defensive" as a separate individual player order, so that's
     * how we'll parse and use it as well. */
    TECH_DEFENSIVE
};

enum class PlayerSkill {
    NONE,   // used for 'uninitialized' values and to denote parse errors
    GOALKEEPING,
    DEFENDING,
    PLAYMAKING,
    WINGER,
    PASSING,
    SCORING,
    SET_PIECES
};

#endif // ENUMS_H
