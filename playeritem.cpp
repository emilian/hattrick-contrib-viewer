/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#include "playeritem.h"

#include <QLinkedList>
#include <QPainter>
#include <QPixmap>
#include <QSvgRenderer>

#include <QtDebug>

#include "enumconvertor.h"
#include "signalemitter.h"

SignalEmitter PlayerItem::emitter;

SignalEmitter* PlayerItem::getEmitter()
{
    return &emitter;
}

PlayerItem::PlayerItem(const PlayerPosition pos,
                       const PlayerSide side,
                       const QLinkedList<PlayerIndividualOrder> &applicableIndividualOrders) :
        isPressed(false),
        position(pos),
        side(side),
        applicableIndividualOrders(applicableIndividualOrders),
        currentlySelectedIndividualOrder(this->applicableIndividualOrders.constBegin()),
        positionText(0),
        boundingRectangle(QRectF(0, 0, 100, 100)),
        playerTextPen(new QPen(QColor(Qt::white))),
        normalPlayerRenderer(new QSvgRenderer(QString(":/gfx/player-normal.svg"))),
        selectedPlayerRenderer(new QSvgRenderer(QString(":gfx/player-selected.svg")))
{
    updatePositionText();
}

PlayerItem::~PlayerItem()
{
    delete positionText;
    positionText = nullptr;
    delete playerTextPen;
    playerTextPen = nullptr;
    delete normalPlayerRenderer;
    normalPlayerRenderer = nullptr;
    delete selectedPlayerRenderer;
    selectedPlayerRenderer = nullptr;
}

QRectF PlayerItem::boundingRect() const
{
    return boundingRectangle;
}

void PlayerItem::paint(QPainter *painter, const QStyleOptionGraphicsItem */* option */, QWidget */* widget */)
{
    if (isPressed) {
        normalPlayerRenderer->render(painter, boundingRectangle);
    } else {
        selectedPlayerRenderer->render(painter, boundingRectangle);
    }

    painter->setPen(*playerTextPen);
    painter->drawText(QPointF(0.0,0.0), *positionText);

}

PlayerIndividualOrder PlayerItem::getIndividualOrder() const
{
    return *currentlySelectedIndividualOrder;
}

PlayerPosition PlayerItem::getPosition() const
{
    return position;
}

PlayerSide PlayerItem::getSide() const
{
    return side;
}

void PlayerItem::deselect()
{
    isPressed = false;
    update();
}

void PlayerItem::mousePressEvent(QGraphicsSceneMouseEvent */* event */)
{
    if (isPressed) {
        currentlySelectedIndividualOrder++;
        if (currentlySelectedIndividualOrder == applicableIndividualOrders.constEnd()) {
            currentlySelectedIndividualOrder = applicableIndividualOrders.constBegin();
        }
        updatePositionText();
    } else {
        isPressed = !isPressed;
    }

    emitter.emitRatingsChanged(this);
    update();
}

void PlayerItem::updatePositionText()
{
    delete positionText;

    positionText = new QString(EnumConvertor::toString(position));
    if (position != PlayerPosition::GOALKEEPER) {
        /* there's only one GK, and there are no positional GK orders,
            so a display of (e.g.) 'GKC (Normal)' would be redundant. */
        positionText->append(EnumConvertor::toString(side));
        positionText->append(" ");
        positionText->append(EnumConvertor::toString(getIndividualOrder()));
    }
}
