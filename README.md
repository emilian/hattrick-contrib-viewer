# hattrick-contrib-viewer

Simple, dumb, minimal player contribution viewer for Hattrick (http://www.hattrick.org/).

Allows seeing how much a player contributes to all areas of the field, depending on his field position and given individual order. Contains the same information presented in the official table (http://bit.ly/1V0MVFD). License: GNU GPLv3.

 - Pitch graphics have been obtained for free from http://cliparts.co/cartoon-football-stadium.
 - Individual player icon made by Freepik (http://www.freepik.com/) from http://www.flaticon.com and licensed under a Creative Commons BY 3.0 license (https://creativecommons.org/licenses/by/3.0/).

Example screenshots:
 - https://gitlab.com/emilian/hattrick-contrib-viewer/raw/master/screenshots/initial-screen.png
 - https://gitlab.com/emilian/hattrick-contrib-viewer/raw/master/screenshots/gk-ratings.png
 - https://gitlab.com/emilian/hattrick-contrib-viewer/raw/master/screenshots/fw-defensive-ratings.png

### Build

.pro file can be loaded into and built directly from the Qt Creator tool. Can also be built from the command line via qmake.
