/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef PITCHAREAHIGHLIGHT_H
#define PITCHAREAHIGHLIGHT_H

#include <QGraphicsObject>

#include "enums.h"
#include "structs.h"

/* QT_BEGIN/END_NAMESPACE help in the case that the Qt library
 *  used to compile the project has itself been compiled with the
 *  '--qtnamespace' option (which allows Qt to be built inside a
 *  user-defined namespace. */
QT_BEGIN_NAMESPACE
class QPen;
QT_END_NAMESPACE

/* Represents the highlight that appears on one area of the pitch.
 * Shifts color to reflect the currently selected player's contribution
 * to that particular area.
 *
 * (e.g. the area corresponding to the central defense will be 100% green
 *  whenever one of the CDs (w/o any specific individual order) is selected,
 *  since the CD's contribution to central defense is 100%). */
class PitchAreaHighlight : public QGraphicsObject
{
    Q_OBJECT
public:
    PitchAreaHighlight(const PitchArea pitchArea, const QRectF& pitchBoundingRect);
    ~PitchAreaHighlight();

    QRectF boundingRect() const override;
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    /* Clears the currently-existing highlight, making the area display as if
     * it did not benefit from any contributions at all. */
    void clear();

    /* Makes the area reflect/display the contributions specified in
     * @activeContributions. */
    void setActiveContributions(const QList<Contribution>& activeContributions);

private:
    PitchArea pitchArea;
    QList<Contribution> activeContributions;
    QRectF boundingRectangle;

    /* Renders text which shows details of player's contribution to this area
     * (contributing skill and value of respective contribution). */
    QPen *contributionDetailPen;
};

#endif // PITCHAREAHIGHLIGHT_H
