#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T22:13:18
#
#-------------------------------------------------

QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hattrick-contrib-viewer
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
    mainwindow.cpp \
    playeritem.cpp \
    signalemitter.cpp \
    ratingmapper.cpp \
    enumconvertor.cpp \
    enumconvertorinnards.cpp \
    pitchareahighlight.cpp

HEADERS  += mainwindow.h \
    playeritem.h \
    enums.h \
    structs.h \
    signalemitter.h \
    ratingmapper.h \
    enumconvertor.h \
    enumconvertorinnards.h \
    pitchareahighlight.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
