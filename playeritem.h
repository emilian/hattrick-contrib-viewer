/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERITEM_H
#define PLAYERITEM_H

/* Saves a bit of headache (we'd need to forward-declare and use new/delete
 * for both QLinkedList and a corresponding QLinkedList::iterator). */
#include <QLinkedList>
#include <QGraphicsItem>
#include <QtGlobal>     // needed for QT_BEGIN/END_NAMESPACE macros

#include "structs.h"

/* QT_BEGIN/END_NAMESPACE help in the case that the Qt library
 *  used to compile the project has itself been compiled with the
 *  '--qtnamespace' option (which allows Qt to be built inside a
 *  user-defined namespace. */
QT_BEGIN_NAMESPACE
class QPen;
class QString;
class QSvgRenderer;
QT_END_NAMESPACE

class SignalEmitter;

/* A player's graphical representation on the pitch.
 *
 * We are using an intermediate class (SignalEmitter) for our signal emitting needs.
 * We could have subclassed QGraphicsObject (which subclasses QObject, and provides
 * intrinsic signal/slot functionality).
 *
 * Even if QGraphicsItem is supposed to have slightly better performance, that was not
 * the main reason for subclassing it. This was done for didactical purposes only. */
class PlayerItem : public QGraphicsItem
{
public:
    /* Returns a pointer to the static SignalEmitter instance.
     * Since this instance's lifetime is supposed to be equal to the program's lifetime,
     *  this should be safe. */
    static SignalEmitter* getEmitter();

    /* Most individual orders are not applicable for all positions.
     * (e.g. a Central Defender cannot be instructed to play 'defensively')
     *
     * We need an external entity to supply a list of which individual orders the player
     * can expect to receive, because PlayerItem is not responsible for parsing the data table
     * (which decides which individual orders are applicable where).
     */
    PlayerItem(const PlayerPosition pos,
               const PlayerSide side,
               const QLinkedList<PlayerIndividualOrder>& applicableIndividualOrders);
    ~PlayerItem();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    PlayerIndividualOrder getIndividualOrder() const;
    PlayerPosition getPosition() const;
    PlayerSide getSide() const;

    /* Used to manually inform the PlayerItem object that its representation on the pitch
     * has been de-selected.
     * Can be used, for example, whenever a different player gets selected by the user. */
    void deselect();

    void setAllowedPlayerIndividualOrders(const QList<PlayerIndividualOrder> orders);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
    /* Updates the text that will be displayed when showing the player's position/side/indiv.order
     * e.g. text can change as a result of the player individual order being changed. */
    void updatePositionText();

    static SignalEmitter emitter;

    bool isPressed;
    PlayerPosition position;
    PlayerSide side;

    QLinkedList<PlayerIndividualOrder> applicableIndividualOrders;
    QLinkedList<PlayerIndividualOrder>::const_iterator currentlySelectedIndividualOrder;

    /* Used for displaying the player's positional details.
     * Basically derived from position + side + individual order. */
    QString *positionText;

    QRectF boundingRectangle;
    QPen *playerTextPen;    // used for rendering text which shows a player's position/side
    QSvgRenderer *normalPlayerRenderer;
    QSvgRenderer* selectedPlayerRenderer;

};

#endif // PLAYERITEM_H
