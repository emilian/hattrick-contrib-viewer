/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef RATINGMAPPER_H
#define RATINGMAPPER_H

#include <QLinkedList>

#include "structs.h"

// Forward-declaring templated classes is frustrating, so let's just include QHash.
#include <QHash>
// We need QString for parser code.
#include <QString>

/* Provides the mapping from a player's [position, side, instruction] to
 *  a list of all ratings contributed by the player to various areas of the field. */
class RatingMapper
{
public:
    /* Constructs a RatingMapper which will retrieve its data from a file stored
     * under 'dataFilename'. */
    RatingMapper(const QString& dataFilename);

    // Returns whether the rating data file was opened successfully.
    bool fileOpenedSuccessfully() const;

    QLinkedList<Contribution> getContributionForPlayer(const PlayerPosition position,
                                                       const PlayerSide side,
                                                       const PlayerIndividualOrder order) const;

    /* Most individual orders are not applicable for all positions.
     * (e.g. a Central Defender cannot be instructed to play 'defensively')
     *
     * This function is used to retrieve a list of individual orders that a player playing in
     * (position, side) can expect to receive. */
    QLinkedList<PlayerIndividualOrder> getApplicableIndividualOrders(const PlayerPosition position,
                                                                     const PlayerSide side) const;
private:
    bool fileOpenedOk;

    /* A player's position + side + order will be condensed down to a QString,
     * which will then be turned into a key under which the corresponding
     * Contribution object will be stored. */
    QHash<QString, QLinkedList<Contribution> > dataTable;

    /* Used for storing which orders are applicable for each individual (position, side)
     * combination. Needed because most individual orders are not applicable for all positions.
     * (e.g. a Central Defender cannot be instructed to play 'defensively')
     *
     * Keying is done similarly to what's being done for the main data table.
     */
    QHash<QString, QLinkedList<PlayerIndividualOrder> > applicableIndividualOrdersTable;

    /* Returns the key under which contributions are stored for the player playing
     * at (@pos, @side), given the order @order. */
    QString getDataTableKey(const PlayerPosition pos,
                            const PlayerSide side,
                            const PlayerIndividualOrder order) const;

    /* Returns the key under which applicable player individual orders are stored for
     * the player playing at (@pos, @side). */
    QString getIndividualOrderTableKey(const PlayerPosition pos, const PlayerSide side) const;


    /* Parser-related members */

    /* How many elements (comma-separated fields) we expect each line to have. */
    static const int EXPECTED_LINE_ELEMENT_COUNT = 11;

    /* Parses the given data line, adding its contents to the data table.
     *
     * A player's contributions are spread across multiple lines (each corresponding to
     * one contributing skill), but only the first one contains the actual player description;
     * the ones after it have a blank player description.
     *
     * e.g. both lines below belong to the same player (the Left CD, when played offensively):
     *  Left CD,Offensive,Defending,0%,73%,40%,0%,0%,0%,0%,
     *  ,,Playmaking,0%,0%,0%,40%,0%,0%,0%,
     *
     * For parsing lines similar to the second one in the example, we have to
     * store parser state in class fields. This state must be cleared once the file has been
     * completely parsed.
     */
    void parseLine(const QByteArray& line);

    /* Used by the file parser function above to store state. */
    PlayerIndividualOrder lastParsedIndivOrder;
    PlayerPosition lastParsedPosition;
    PlayerSide lastParsedSide;
};

#endif // RATINGMAPPER_H
