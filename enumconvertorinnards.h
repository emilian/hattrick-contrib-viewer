/*
 * Copyright (C) 2016 devemilian-at-gmail.com
 *
 * This file is part of hattrick-contrib-viewer
 *
 * hattrick-contrib-viewer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA, or see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef PARSERINNARDS_H
#define PARSERINNARDS_H

#include "enums.h"

#include <QHash>
#include <QString>

class EnumConvertorInnards
{
public:
    EnumConvertorInnards();

    QString toString(const PlayerIndividualOrder order);
    QString toString(const PlayerPosition position);
    QString toString(const PlayerSide side);
    QString toString(const PlayerSkill skill);

    PlayerIndividualOrder ordFromString(const QString& string);
    PlayerPosition posFromString(const QString& string);
    PlayerSide sidFromString(const QString& string);
    PlayerSkill sklFromString(const QString& string);

    /* Functions which add a bi-directional mapping between an enum value
     * and its corresponding string representation. */
    void addMapping(const PlayerIndividualOrder order, const QString& string);
    void addMapping(const PlayerPosition position, const QString& string);
    void addMapping(const PlayerSide side, const QString& string);
    void addMapping(const PlayerSkill skill, const QString& string);

private:
    /* QMap/QHash do not offer any fast way of performing a reverse lookup,
     * so we'll just use two mappings for each enum value.
     * We're basically emulating Boost's 'bimap' class here, since we
     * need to be able to both (1) quickly produce an enum value's string
     * representation and (2) try to parse a given string to its corresponding
     * enum value.
     *
     * Any addition/removal of an enum value must keep these tables in sync.
     * (e.g. for every [key, value] pair inserted in 'hashOrdStr', the corresponding
     * [value, key] pair must be inserted in 'hashStrOrd'. */
    QHash<PlayerIndividualOrder, QString>   hashOrdStr;
    QHash<PlayerPosition, QString>          hashPosStr;
    QHash<PlayerSide, QString>              hashSidStr;
    QHash<PlayerSkill, QString>             hashSklStr;

    QHash<QString, PlayerIndividualOrder>   hashStrOrd;
    QHash<QString, PlayerPosition>          hashStrPos;
    QHash<QString, PlayerSide>              hashStrSid;
    QHash<QString, PlayerSkill>             hashStrSkl;
};

#endif // PARSERINNARDS_H
